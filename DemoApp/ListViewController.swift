import UIKit

final class ListViewController: UIViewController {
    enum Row: String, CaseIterable {
        case inputText = "Input Text"
        case slider = "Slider"
        case dragAndDrop = "Drag And Drop"
        case mapView = "Map View"
        case playVideo = "Play Video"
        case webView = "Web View"
        case viewPager = "View Pager"
        case gridView = "Grid View"
        case unquete = "Unquete"
        case scroll = "Scroll"
    }

    @IBOutlet private weak var listTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        listTableView.dataSource = self
        listTableView.delegate = self

        setUpNavigationItem()
        setUpNavigationBar()
    }

    private func setUpNavigationItem() {
        navigationItem.title = "Demo App"
        navigationItem.backBarButtonItem = UIBarButtonItem()
    }

    private func setUpNavigationBar() {
        navigationController?.navigationBar.barTintColor = .blue
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    }
}

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Row.allCases.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = Row.allCases[indexPath.row].rawValue
        return cell
    }
}

extension ListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = Row.allCases[indexPath.row]
        switch row {
        case .inputText: break
        case .slider:
            guard let sliderViewController = UIStoryboard(name: "Slider", bundle: nil).instantiateViewController(withIdentifier: "Slider") as? SliderViewController else { return }
            navigationController?.pushViewController(sliderViewController, animated: true)
        case .dragAndDrop: break
        case .mapView: break
        case .playVideo: break
        case .webView: break
        case .viewPager: break
        case .gridView: break
        case .unquete: break
        case .scroll: break
        }
    }
}
