import UIKit

final class SliderViewController: UIViewController {
    private var redValue = CGFloat(0.0)
    private var greenValue = CGFloat(0.0)
    private var blueValue = CGFloat(0.0)

    @IBOutlet private weak var colorView: UIView!
    @IBOutlet private weak var redColorSlider: UISlider!
    @IBOutlet private weak var greenColorSlider: UISlider!
    @IBOutlet private weak var blueColorSlider: UISlider!

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpNavigationItem()
        reloadColorView()
    }

    @IBAction private func onChangedColorSlider(_ sender: UISlider) {
        switch sender {
        case redColorSlider:
            redValue = CGFloat(sender.value)
        case greenColorSlider:
            greenValue = CGFloat(sender.value)
        case blueColorSlider:
            blueValue = CGFloat(sender.value)
        default:
            break
        }
        reloadColorView()
    }

    private func setUpNavigationItem() {
        navigationItem.title = "Slider Sample"
    }

    private func reloadColorView() {
        colorView.backgroundColor = UIColor(red: redValue, green: greenValue, blue: blueValue, alpha: 1.0)
    }
}
